/*

 Para compilar, desde la terminal:

                    javac Main.java

 Para correr, desde la terminal:

                    java Main < 1.in
 */
 
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        
        //boolean[] primo = criba(50);
        
        // Tabla precalculada.
        long[] resultados = {
            0L, 
            0L, 
            2L, 
            6L, 
            6L, 
            30L, 
            30L, 
            210L, 
            210L, 
            210L, 
            210L, 
            2310L, 
            2310L, 
            30030L, 
            30030L, 
            30030L, 
            30030L, 
            510510L, 
            510510L, 
            9699690L, 
            9699690L, 
            9699690L, 
            9699690L, 
            223092870L, 
            223092870L, 
            223092870L, 
            223092870L, 
            223092870L, 
            223092870L, 
            6469693230L, 
            6469693230L, 
            200560490130L, 
            200560490130L, 
            200560490130L, 
            200560490130L, 
            200560490130L, 
            200560490130L, 
            7420738134810L, 
            7420738134810L, 
            7420738134810L, 
            7420738134810L, 
            304250263527210L, 
            304250263527210L, 
            13082761331670030L, 
            13082761331670030L, 
            13082761331670030L, 
            13082761331670030L, 
            614889782588491410L, 
            614889782588491410L, 
            614889782588491410L, 
            614889782588491410L 
        };
        /*
        long r = 1;
        for (int i = 2; i <= 50; i++) {
            if (primo[i]) {
                r *= i;
            }
            resultados[i] = r;
        }
        for (int i = 0; i <= 50; i++) {
            System.out.println(resultados[i] + "L, ");
        }
        */
        
        while ((n = in.nextInt()) != 0) {
            System.out.println(resultados[n]);
        }
        
        in.close();
    }
    
    /*
    public static boolean[] criba(int n) {
        boolean[] r = new boolean[n + 1];
        Arrays.fill(r, true);
        r[0] = false;
        r[1] = false;
        for (int i = 2; i < n; i++) {
            if (!r[i]) continue;
            for (int j = i * i; j <= n; j += i) {
                r[j] = false;
            }
        }
        return r;
    }
    */
}
