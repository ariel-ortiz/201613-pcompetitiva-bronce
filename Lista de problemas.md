# Lista de problemas

- 31 de octubre, 2016.
    * [COJ_2434 -- Mathematician Ana](http://coj.uci.cu/24h/problem.xhtml?pid=2434)
    * [COJ_2427 --  How Many Primes?](http://coj.uci.cu/24h/problem.xhtml?pid=2427)

- 24 de octubre, 2016.
    * [COJ_1050 -- Coprimes](http://coj.uci.cu/24h/problem.xhtml?pid=1050) 
    * [COJ_2843 -- GCD](http://coj.uci.cu/24h/problem.xhtml?pid=2843)

- 10 de octubre, 2016.
    * [COJ_2413 -- Div 5](http://coj.uci.cu/24h/problem.xhtml?pid=2413)
    * [COJ_1297 -- Divisibility by 495](http://coj.uci.cu/24h/problem.xhtml?pid=1297)
    * [COJ_2382 -- Elvis the Squirrel](http://coj.uci.cu/24h/problem.xhtml?pid=2382)
    * [COJ_2307 -- Div 3 Roman](http://coj.uci.cu/24h/problem.xhtml?pid=2307)
    * [COJ_1051 -- Div 3](http://coj.uci.cu/24h/problem.xhtml?pid=1051)