/*

 Para compilar, desde la terminal:

                    javac Primos.java

 Para correr, desde la terminal:

                    java Primos
 */
 
import java.util.Arrays;

public class Primos {

    public static void main(String[] args) {
        for (int i = 0; i <= 10; i++) {
            if (esPrimo(i)) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
        boolean[] r = criba(1500);
        int suma = 0;
        for (int i = 1000; i <= 1500; i++) {
            if (r[i]) {
                suma += i;
            }
        }
        System.out.println(suma);
    }

    public static boolean esPrimo(int n) {
        if (n < 2) {
            return false;
        }
        int max = (int) Math.sqrt(n);
        for (int i = 2; i <= max; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean[] criba(int n) {
        boolean[] r = new boolean[n + 1];
        Arrays.fill(r, true);
        r[0] = false;
        r[1] = false;
        for (int i = 2; i < n; i++) {
            if (!r[i]) continue;
            for (int j = i * i; j <= n; j += i) {
                r[j] = false;
            }
        }
        return r;
    }

}
