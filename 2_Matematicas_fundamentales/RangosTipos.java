/*

 Para compilar, desde la terminal:

                    javac RangosTipos.java

 Para correr, desde la terminal:

                    java RangosTipos
 */

public class RangosTipos {

    public static void main(String[] args) {
        System.out.printf("int %.2E ... %.2E%n",
                (double) Integer.MIN_VALUE,
                (double) Integer.MAX_VALUE);
        System.out.printf("long %.2E ... %.2E%n",
                (double) Long.MIN_VALUE,
                (double) Long.MAX_VALUE);
        System.out.printf("float %.2E ... %.2E%n",
                Float.MIN_VALUE,
                Float.MAX_VALUE);
        System.out.printf("double %.2E ... %.2E%n",
                Double.MIN_VALUE,
                Double.MAX_VALUE);
        System.out.println(1.0/0.0);
        System.out.println(-1.0/0.0);
        System.out.println(0.0/0.0);

        double x = 0.0/0.0;
        if (x == x) {
            System.out.println("NaN es igual a sí mismo");
        }
    }
}
