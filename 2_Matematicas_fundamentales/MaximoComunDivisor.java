/*

 Para compilar, desde la terminal:

                    javac MaximoComunDivisor.java

 Para correr, desde la terminal:

                    java MaximoComunDivisor
 */
 
 
public class MaximoComunDivisor {

    public static void main(String[] args) {
        System.out.println(gcd(25, 35));
        System.out.println(gcd(666, 150));
        System.out.println(gcd(15, 15));
        System.out.println(gcd(30, 20, 25));
        int a[] = {30, 50, 80, 20, 40};
        System.out.println(gcd(a));
        int b[] = {27, 81, 18, 54, 36};
        System.out.println(gcd(b));
        System.out.println(lcm(20, 30));
        System.out.println(lcm(20, 30, 40));
    }
    
    public static int gcd(int x, int y) {
        while (y != 0) {
            int t = y;
            y = x % y;
            x = t;
        }
        return x;
    }
    
    public static int gcd(int x, int y, int z) {
        return gcd(gcd(x, y), z);
    }
    
    public static int gcd(int[] a) {
        int r = a[0];
        for (int i = 1; i < a.length; i++) {
            r = gcd(r, a[i]);
        }
        return r;
    }
    
    public static int lcm(int x, int y) {
        return (x * y) / gcd(x, y);
    }
    
    public static int lcm(int x, int y, int z) {
        return lcm(lcm(x, y), z);
    }
}
