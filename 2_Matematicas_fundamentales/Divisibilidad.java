/*

 Para compilar, desde la terminal:

                    javac Divisibilidad.java

 Para correr, desde la terminal:

                    java Divisibilidad
 */

import java.util.Scanner;

public class Divisibilidad {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        String cadena = entrada.nextLine();
        System.out.println("Divisible entre 2: " + divisibleEntre2(cadena));
        System.out.println("Divisible entre 3: " + divisibleEntre3(cadena));
        System.out.println("Divisible entre 4: " + divisibleEntre4(cadena));
        System.out.println("Divisible entre 5: " + divisibleEntre5(cadena));
        System.out.println("Divisible entre 6: " + divisibleEntre6(cadena));
        System.out.println("Divisible entre 7: " + divisibleEntre7(cadena));
        System.out.println("Divisible entre 8: " + divisibleEntre8(cadena));
        System.out.println("Divisible entre 11: " + divisibleEntre11(cadena));
        entrada.close();
    }

    private static boolean divisibleEntre2(String cadena) {
        char ultimo = cadena.charAt(cadena.length() - 1);
        return ultimo % 2 == 0;
    }

    private static boolean divisibleEntre3(String cadena) {
        int suma = 0;
        for (char c: cadena.toCharArray()) {
            suma += c - 48;
        }
        return suma % 3 == 0;
    }

    public static boolean divisibleEntre4(String cadena) {
        String num;
        if (cadena.length() < 2) {
            num = cadena;
        } else {
            num = cadena.substring(cadena.length() - 2,
                cadena.length());
        }
        int n = Integer.parseInt(num);
        return n % 4 == 0;
    }

    private static boolean divisibleEntre5(String cadena) {
        char ultimo = cadena.charAt(cadena.length() - 1);
        return ultimo == '0' || ultimo == '5';
    }

    private static boolean divisibleEntre6(String cadena) {
        return divisibleEntre2(cadena) && divisibleEntre3(cadena);
    }

    private static boolean divisibleEntre7(String cadena) {
        int[] secuencia = {1, 3, 2, 6, 4, 5};
        long suma = 0;
        for (int i = cadena.length() - 1, j = 0;
                i >= 0;
                i--, j = (j + 1) % 6) {
            suma += (cadena.charAt(i) - '0') * secuencia[j];
        }
        return suma % 7 == 0;
    }

    public static boolean divisibleEntre8(String cadena) {
        String num;
        if (cadena.length() < 3) {
            num = cadena;
        } else {
            num = cadena.substring(cadena.length() - 3,
                cadena.length());
        }
        int n = Integer.parseInt(num);
        return n % 8 == 0;
    }

    private static boolean divisibleEntre11(String cadena) {
        int suma = 0;
        int signo = 1;
        for (char c: cadena.toCharArray()) {
            suma += (c - 48) * signo;
            signo *= -1;
        }
        return suma % 11 == 0;
    }
}
