# COJ: 1000 - A+B Problem

http://coj.uci.cu/24h/problem.xhtml?pid=1000

Compilación:

    javac Main.java

Corridas:

    $ java Main < 1.in
    3

    $ java Main < 2.in
    7

    $ java Main < 3.in
    20

    $ java Main < 4.in
    0