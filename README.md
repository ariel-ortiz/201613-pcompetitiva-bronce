# Programación competitiva -- División de bronce

Este repositorio contiene los archivos correspondientes a la división de bronce del grupo de programación competitiva del Tecnológico de Monterrey, Campus Estado de México, correspondientes al semestre agosto-diciembre del 2016.

## Temas

1. Introducción
    * ICPC de la ACM
    * Jueces en línea
        * [Sphere Online Judge (SPOJ)](http://www.spoj.com/)
        * [Caribbean Online Judge (COJ)](http://coj.uci.cu/)
        * [omegaUp](https://omegaup.com/)
    * Ambiente de desarrollo para Java
2. Matemáticas fundamentales
    * Rangos de tipos primitivos numéricos de Java
    * Reglas de divisibilidad
        * División y módulo con enteros.
        * División con reales.
        * Números divisibles entre 2.
    * Suma de números consecutivos
    * Suma de cuadrados consecutivos
    * Suma de cubos consecutivos
